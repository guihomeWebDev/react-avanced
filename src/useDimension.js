import {useState, useEffect} from 'react'

export default function useDimension() {


  const [dimension, setDimension] = useState()

  useEffect(() => {
      window.addEventListener('resize', handleResize);

      function handleResize() {
            setDimension({
                width: window.innerWidth,
                height: window.innerHeight
            });
      }
      handleResize();
      return () => {
            window.removeEventListener('resize', handleResize);
        }
  }, []);
    return dimension;
}
