import React from 'react'

export default function Children(props) {
  return (
    <div className='content'>
        {props.children}
    </div>
  )
}
