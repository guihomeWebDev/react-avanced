import { useState, useEffect } from 'react';
import React from 'react'

export default function Timer() {

    const [timer, setTimer] = useState(1);

     useEffect(() => {
    const interval = setInterval(() => {
      setTimer(timer + 1);
    }, 1000);
    return () => clearInterval(interval);
  }, [timer]);


  return (
    <>
        <h1>timer : {timer}</h1>
    </>
  )
}
