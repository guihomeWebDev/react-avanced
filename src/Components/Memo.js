import React from 'react'

 function Memo({num}) {
  return (
    <div>
        <h1>{num}</h1>
    </div>
  )
}

export default React.memo(Memo);