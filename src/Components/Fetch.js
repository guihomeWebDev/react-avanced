import { useState, useEffect } from 'react';
import React from 'react'

export default function Fetch() {
    
    const [dataComponent, setDataComponent] = useState(1);

    useEffect(() => {
    fetch('https://api.thecatapi.com/v1/images/search')
    .then(response => response.json())
    .then(data => {
      setDataComponent(data[0].url);
    })
  }, []);

  return (
    <div>
       {dataComponent && <img src={dataComponent} alt="cat" />}
    </div>
  )
}
