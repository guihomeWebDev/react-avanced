import { useState, useEffect, useRef, useMemo } from 'react';
import './App.css';
import Video from './Video.mp4';
import Children from './Components/Children';
import Memo from './Components/Memo';
import useDimension from './useDimension';

function App() {
  const [toggle, setToggle] = useState(false);

  const ref = useRef([]);

  useEffect(() => {
  //  setTimeout(() => {
  //     ref.current.pause();
  //   }, 1500);
  console.log(ref);
  }, []);

  const toggleFunc = () => {
    const newTab = [...toggle];
    newTab.push(!toggle);
    setToggle(newTab);
  }

  const addToRef = element => {
    console.log(element);
    if(element && !ref.current.includes(element)) {
      ref.current.push(element);
    }
  }

  const data = useMemo(() => {
    return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  }, []);

  return (
    <div className="App">
      <Memo num={2} />
      <Memo num={data} />
      <Children>
        <h1>titre de mon article 1</h1>
        <p>Lorem ipsum dolor sit amet.</p>        
      </Children>
      <Children>
        <h1>titre de mon article 2</h1>
        <p>Lorem ipsum dolor sit amet.</p>        
      </Children>
      <Children>
        <h1>titre de mon article 3</h1>
        <p>Lorem ipsum dolor sit amet.</p>        
      </Children>
      <video ref={addToRef} width="750" height="500" autoPlay controls muted>
        <source src={Video} type="video/mp4" />
      </video>
     
      <button onClick={toggleFunc}>Toggle</button>
    </div>
  );
}

export default App;
